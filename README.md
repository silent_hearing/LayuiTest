# LayuiTest

#### 介绍
丁零当啷系统前端部分

演示地址：http://dldl.vipgz1.idcfengye.com/LayuiTest

后端项目仓库地址：https://gitee.com/silent_hearing/jingleRinging

#### 软件架构
使用了驊驊龔頾的layuicms2.0框架

地址：https://gitee.com/layuicms/layuicms2.0

### 软件需求

HBuilder X或nginx等更改提供服务器功能的工具，

因为项目中有些资源文件是根据服务器地址加载。

### 本地部署

用HBuilder X打开LayuiTest文件夹，

更改custom/js/common.js里的common对象

后端项目运行后，用HBuilder X运行login.html

后端项目仓库地址：https://gitee.com/silent_hearing/jingleRinging